local sensorInfo = {
	name = "MakeFormation",
	desc = "Distributes units equally to given positions.",
	author = "novotnyt",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spCommand = Spring.GiveOrderToUnit
local spGetUnitPosition = Spring.GetUnitPosition 

-- @description return current wind statistics
return function(pickedUnit)       
  if #units > 0 and pickedUnit ~= nil then
    spCommand(units[1], CMD.LOAD_UNITS, {pickedUnit}, {})
    spCommand(pickedUnit, CMD.MOVE, {spGetUnitPosition(units[1])}, {})
  end
end