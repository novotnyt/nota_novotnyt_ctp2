local sensorInfo = {
	name = "GetHills",
	desc = "Return centers of all hills on map.",
	author = "novotnyt",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spGetUnitPosition = Spring.GetUnitPosition
local spGetHeight = Spring.GetGroundHeight
-- @description return current wind statistics

return function()
  JUMP = 10
	width = Game.mapSizeX
  height = Game.mapSizeZ
  spEcho("Map size:", width, height)
  hills = {}
  hillCnt = 0         
  
  --get bit map of terrain height  
  groundMap = {}  
  for w = 1, width/JUMP do
    groundMap[w] = {}
    for h = 1, height/JUMP do
      ground = spGetHeight(w*JUMP,h*JUMP)
      if ground > 190 then
        groundMap[w][h] = true
      else
        groundMap[w][h] = false
      end
    end
  end
  
  --for each connected component, returns its size and coordinate sums
  local function findComponent(baseW, baseH)   
    local count = 0
    local sumW = 0
    local sumH = 0
    local curW = baseW
    
    --use all points in horizontal line
    while groundMap[curW][baseH] do
      curW = curW - 1
    end
    curW = curW + 1
    local startW = curW
    while groundMap[curW][baseH] do
      groundMap[curW][baseH] = false
      count = count + 1
      sumW = sumW + curW
      sumH = sumH + baseH
      curW = curW + 1
    end
    local endW = curW - 1
    
    --fill both sides recursively  
    for curW = startW, endW do 
      if groundMap[curW][baseH-1] then
        nc, nw, nh = findComponent(curW, baseH-1)
        count = count + nc
        sumW = sumW + nw
        sumH = sumH + nh 
      end     
      if groundMap[curW][baseH+1] then
        nc, nw, nh = findComponent(curW, baseH+1)
        count = count + nc
        sumW = sumW + nw
        sumH = sumH + nh 
      end       
    end
    
    return count, sumW, sumH
  end
  
  --find all hills 
  for w = 1, width/JUMP do
    for h = 1, height/JUMP do
      if groundMap[w][h] then
        --spEcho(w, h)
        hillCnt = hillCnt + 1
        size, sumW, sumH = findComponent(w, h)
        spEcho("Hill:", size, sumW/size, sumH/size)
        hills[hillCnt] = {x = sumW*JUMP/size, z = sumH*JUMP/size}
      end
    end
  end
  return hills
end