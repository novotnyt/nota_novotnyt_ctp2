local sensorInfo = {
	name = "MakeFormation",
	desc = "Distributes units equally to given positions.",
	author = "novotnyt",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spCommand = Spring.GiveOrderToUnit

-- @description return current wind statistics
return function(locations)       
  for i = 2, #units do
    loc = locations[((i - 2) % #locations) + 1]
    spCommand(units[i], CMD.MOVE, {loc.x, 190, loc.z}, {})
  end
end